package org.sber.exercise3;

import java.util.*;

public class Main {

    public static void counterCharacter(String str) {
        Map<String, Integer> map = new HashMap<>();
        for (String symbol : str.toUpperCase().split("")){
            if (symbol.equals(" ")) {
                continue;
            }
            Integer value = map.getOrDefault(symbol, 0);
            map.put(symbol, ++value);
        }
        map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEach(entry -> System.out.println(entry.getKey() + ": " + entry.getValue()));
    }

    public static void main(String[] args) {
        counterCharacter("Как у тебя дела?");
    }
}


