package org.sber.exercise1;

import java.util.Iterator;
import java.util.NoSuchElementException;

// создано на основе Iterator в ArrayList
public class StringIterator implements Iterator<String> {

    private String[] array;
    private int cursor; // индекс следующего возвращаемого элемента
    private int lastRet = -1; // индекс последнего возвращенного элемента; -1 если нет такого

    private StringIterator(String[] array) {
        this.array = array;
    }

    public static StringIterator getIterator(String[] array){
        return new StringIterator(array);
    }

    @Override
    public boolean hasNext() {
        return cursor != array.length;
    }

    @Override
    public String next() {
        if (cursor >= array.length)
            throw new NoSuchElementException();
        lastRet = cursor;
        cursor++;
        return array[lastRet];
    }
}
