package org.sber.exercise2;

import org.junit.Test;

import static org.junit.Assert.*;

public class ReversePolishNotationConverterTest {

    @Test
    public void toRPN() {
        String str1 = "1+2*3";
        String result1 = ReversePolishNotationConverter.toRPN(str1);
        assertEquals(result1, "1 2 3 * +");

        String str2 = "1*2+3";
        String result2 = ReversePolishNotationConverter.toRPN(str2);
        assertEquals(result2, "1 2 * 3 +");

        String str3 = "1*2+3*4";
        String result3 = ReversePolishNotationConverter.toRPN(str3);
        assertEquals(result3, "1 2 * 3 4 * +");

        String str4 = "(1+2)/(3-4)";
        String result4 = ReversePolishNotationConverter.toRPN(str4);
        assertEquals(result4, "1 2 + 3 4 - /");

        String str5 = "1*2/3";
        String result5 = ReversePolishNotationConverter.toRPN(str5);
        assertEquals(result5, "1 2 * 3 /");
    }
}