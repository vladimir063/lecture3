package org.sber.exercise2;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ReversePolishNotationConverter {

    public static String toRPN(String str){
        if (str.isEmpty()) {
            throw  new NumberFormatException("Введена пустая строка");
        }
        List<String> strings = strToList(str);  // преобразуем строку в лист, для того чтобы корректно расставить пробелы потом в результате
        Stack<String> stack = new Stack<>();
        List<String> result = new ArrayList<>();
        for (String t : strings) {
            if (isDigit(t)) {  // Если токен — число, то добавить его в очередь вывода.
                result.add(t);
            } else if(t.equals("+") || t.equals("-")  || t.equals("*") || t.equals("/") ) {   // Если токен — оператор op1, то:
                while (!stack.isEmpty() && getPriority(stack.peek()) >= getPriority(t)) {  // Пока присутствует на вершине стека токен оператор op2,
                                                                                               // чей приоритет выше или равен приоритету op1
                    result.add(stack.pop());  // Переложить op2 из стека в выходную очередь;
                }
                stack.push(t); // Положить op1 в стек.
            }  else  if (t.equals("(")) {     // Если токен — открывающая скобка, то положить его в стек.
                stack.add(t);
            }
            else  if (t.equals(")")) {   // Если токен — закрывающая скобка:
                while (!stack.peek().equals("(")) {  //Пока токен на вершине стека не открывающая скобка
                    result.add(stack.pop());   //Переложить оператор из стека в выходную очередь.
                }
                stack.pop();  //  Выкинуть открывающую скобку из стека, но не добавлять в очередь вывода.
            } else{
                throw new NumberFormatException("Неизвестный символ");
            }
        }
        while (!stack.isEmpty()) {    //  Пока есть токены операторы в стеке:
            result.add(stack.pop());  // Переложить оператор из стека в выходную очередь.
        }
       return printResult(result);
    }

    private static int getPriority(String t) {
        switch (t) {
            case "(" : return 0;
            case "+" :
            case "-" : return 1;
            case "*" :
            case "/" : return 2;
            default :  throw new NumberFormatException("Неизвестный символ");
        }
    }

    // конвертер формулы в лист
    private static List<String> strToList(String str){
        List<String> list = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        for (Character c : str.toCharArray()) {
            if (Character.isDigit(c)) {
                stringBuilder.append(c);
                continue;
            }
            if (stringBuilder.length()!=0) {
                list.add(stringBuilder.toString());
                stringBuilder = new StringBuilder();
            }
            list.add(String.valueOf(c));
        }
        if (stringBuilder.length()!=0) {
            list.add(stringBuilder.toString());
        }
        return list;
    }

    private static boolean isDigit(String t){
        for (Character c : t.toCharArray()) {
            if (!Character.isDigit(c)){
                return false;
            }
        }
        return true;
    }

    private static String printResult(List<String> result) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String str : result) {
            stringBuilder.append(str).append(" ");
        }
        return stringBuilder.toString().trim();
    }
}

